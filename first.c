//Instruction 
//Operation
/*
//1.Type declearation instruction
//Variable aga declear korte hobe
#include<stdio.h>
#include<math.h>
int main(){
    int a = 22;
    int a = b = c;//dont use this type
    int a, b, c = 1;// dont use this type
    int e, f, g;
    e = 10;
    f = 20;
    g = e + f;
    //firt declear and then use dont use operation then use

    return 0;
}
*/
/*//2.Arithmetic Instruction
#include<stdio.h>
#include<math.h>
int main(){
    int a = 2, b =2;
    int sum = a + b;
    int multiply = a * b;
    // Variable means L.H.S and all Operation are R.H.S like sum = a+b+c.
    a = a + b;
    int x = 2, y = 3;
    pow(x, y);// y value is power of x.
    a = bc//dont use this multipication.

    return 0;
}
*/
/* power ar programm ayvabe hobe
#include<stdio.h>
#include<math.h>
int main(){
    int x = 2, y = 3;
    int power = pow(x, y);
    printf("%d", power);

    return 0;
}
*/
/*modulo
#include<stdio.h>
#include<math.h>
int main(){
    printf("%d",10 % 3);//modulo cant operation with float value
    //modulo int ar sathe negative value holew kaj kore

    return 0;
}
*/

/*//Type Conversion
// int to int = int
// int to float = float // float container ar jaiga beshi 4 bit tai amon
// float to float = float
#include<stdio.h>
#include<math.h>
int main(){
    printf("%f \n", 2 / 4.00);// exact valu ar jonno 1 ta value must float hote hobe
    printf("%d  \n", 10 / 4);
    int a = (int)1.999999; // implicit korte chai na expit kote kori
    printf("%d \n", a);
    return 0;
}
*/

/*//Operator prcedence
// *,/,% then +,- and then =
// same precedence ar khetre associativity Left to right
#include<stdio.h>
#include<math.h>
int main(){
    //int a = 4 * 3 / 7 * 2;
    int a = 4;
    float b = 3;
    int c = 7;
    int d = 2;
    printf("%f", a*b/c*d);

    return 0;
}
*/
/*//Another example of associativity rules
#include<stdio.h>
#include<math.h>
int main(){
    int a = 5 * (2 / 3.00) * 3;
    printf("%d", a);

    return 0;
}
*/


/*//Controll instruction 4 Type
//1. Sequence Controll...normally ar aga ja ja sikhlam
//2. Decision Controll...If ElSe 
//3. Loop Controll....declear condition itearation 
//4. case Controll ...Swith Case
#include<stdio.h>
#include<math.h>
int main(){

    return 0;
}
*/

/*// Operator:

//a. Arithmetic Operator
//b. Relational Operator
//c. Logical Operator
//d. bitwise Operator
//e. Assigment Operator
//f. Ternary operator if else akbarei likha jai
*/

/* //Relational operator Comparison chek relation
#include<stdio.h>
#include<math.h>
int main(){

    printf("%d \n", 4 != 4);//c programming a boolen nai 1 means ture 0 means false
    printf("%d \n", 5 > 4);

    return 0;
}
*/

/*//Logical Operator &&, ||, ! Check Tow or more condition
#include<stdio.h>
#include<math.h>
int main(){
    printf("%d", !(3 > 4) && (4 < 3));
    return 0;
}
*/


/*// Operator presidence:
// 1. ! 2.*vag% 3.+- 4.<<=>>= 5.==!= 6.&&  7.|| 8.=
*/
/*
// assignment operator;
// i = i+1, i+=1, i++
*/


/*//Practice problem:
//1. Write a program if number divided by 2 or not
#include<stdio.h>
#include<math.h>
int main(){
    printf("%d \n", 11 % 2 == 0);
    return 0;
}
*/

/*// practice problem:
// 2. Some problem Case.

#include<stdio.h>
#include<math.h>
int main(){

    int z = 8 ^ 8;
    printf("%d \n", z);

    int a = 10; int y = a;
    printf("%d \n", a);

    //int b = 10, int c = b; // this is not valid

    char doublestar ='*'; // multichactar space o deouya jabe na
    printf("%c", doublestar);

    return 0;
}
*/

/*//practice problem:
//3.Print 1(true) or 0(False) for following Statement
//a. if it is sunday & it is snowing
//b. it it is monday or it is raining
//c. if  a number is greater than 9 & less than 100.
#include<stdio.h>
#include<math.h>
int main(){
    int isSunday = 1;
    int isSnowing = 1;
    printf("%d", isSunday && isSnowing);

    return 0;
}
*/
/*
#include<stdio.h>
#include<math.h>
int main(){
    int i;
    printf("Enter a number: ");
    scanf("%d", &i);

    printf("%d", 9 < i && i < 100);
    return 0;
}
*/

/*
//Homework:
//a. Write a program to print the average of 3 number.
//b. Write a programm to check it given number is digit or not
//c. write a programm to print the smallest number.
*/
